#!/bin/bash

cd /cluster/home/michalo/scratch/CAMDA
module load new
module load samtools
module load gdc gcc/4.8.2 subread/1.5.0

fn=$1
basefn=${fn}Aligned.out.sam
samtools view -@ 24 -bS ${basefn} > ${fn}.bam
samtools sort -@ 24 ${fn}.bam ${fn}.sorted
samtools index -@ 24 ${fn}.sorted.bam

#/cluster/home/michalo/bin/featureCounts -T 24 -t gene -g gene_id -f -a /cluster/home/michalo/work_michalo/Rnor6/Rnor83.gtf -o ${fn}.cnt ${fn}.sorted.bam
featureCounts -T 24 -t gene -g gene_id -f -M --fraction -a /cluster/home/michalo/work_michalo/Rnor6/Rnor83.gtf -o ${fn}.cnt ${fn}.sorted.bam
#/cluster/home/michalo/bin/featureCounts -T 24 -t gene -g gene_id -f -a /cluster/home/michalo/work_michalo/Rnor6/Rnor83.gtf -o ${fn}.cnt ${fn}.sorted.bam

rm ${fn}.bam
rm ${fn}.sorted.bam

