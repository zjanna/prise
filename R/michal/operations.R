library(gplots)



cc <- read.csv("CAMDA_normal_star_normal_featureCount.csv")
cc_multimap <- read.csv("CAMDA_normal_star_multi_featureCount.csv")
cc_nomismatch <- read.csv("CAMDA_nomm_star_normal_featureCount.csv")


norm_c <- read.table("~/projects/CAMDA/normal_SRR1177990.cnt", skip=1, header=T)
multi_c <- read.table("~/projects/CAMDA/multi_SRR1177990.cnt", skip=1, header=T)
nomm_c <- read.table("~/projects/CAMDA/nomm_SRR1177990.cnt", skip=1, header=T)
fraction_c <- read.table("~/projects/CAMDA/fraction_SRR1177990.cnt", skip=1, header=T)

rownames(norm_c) <- norm_c[,1]
rownames(multi_c) <- multi_c[,1]

norm_c <- norm_c[,7]
names(norm_c) <- rownames(multi_c)
multi_c <- multi_c[,7]
names(multi_c) <- names(norm_c)
nomm_c <- nomm_c[,7]
names(nomm_c) <- names(norm_c)
fraction_c <- fraction_c[,7]
names(fraction_c) <- names(norm_c)

plot(norm_c, multi_c, cex=0.3)
plot(log2(norm_c+0.01), log2(multi_c+0.01), cex=0.3)

plot(log2(norm_c+0.01), log2(nomm_c+0.01), cex=0.3)







